import { prisma } from "../generated/prisma-client";
import { Strategy, ExtractJwt } from "passport-jwt";
import passport from "passport";

const jwtOptions = {
  jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
  secretOrKey: process.env.JWT_KEY,
};

const verifyClient = async (payload, done) => {
  try {
    let client = await prisma.user({ id: payload.id });
    if (client !== null) {
      client.password = "";
      return done(null, client);
    } else {
      return done(null, false);
    }
  } catch (e) {
    console.log(e);
    return done(e, false);
  }
};

export const authenticateJwt = (req, res, next) => {
  return passport.authenticate("jwt", { session: false }, (_, client) => {
    if (client) {
      req.client = client;
    }
    next();
  })(req, res, next);
};

passport.use(new Strategy(jwtOptions, verifyClient));
passport.initialize();
