import { GraphQLServer } from "graphql-yoga";
import express from "express";
import logger from "morgan";

import "./env";
import schema from "./schema";
import userRouter from "./Rapi/User/Router";
import instanceRouter from "./Rapi/App/Instance/Router";
import projectRouter from "./Rapi/App/Project/Router";
import pushRouter from "./Rapi/Push/Router";
import debugRouter from "./Rapi/Debug/Router";
import devopsRouter from "./Rapi/Devops/Router";
import ftpRouter from "./Rapi/Ftp/Router";
import { authenticateJwt } from "./passport";
import { isAuthenticated } from "./utils";

const server = new GraphQLServer({
  schema,
  context: ({ request, connection }) => ({ isAuthenticated, request, connection }),
});

server.express.use(logger("dev"));
server.express.use(express.json());
server.express.use(express.urlencoded({ extended: false }));
server.express.use(authenticateJwt);

server.use("/user", userRouter);
server.use("/app/instance", instanceRouter);
server.use("/app/project", projectRouter);
server.use("/push", pushRouter);
server.use("/debug", debugRouter);
server.use("/devops", devopsRouter);
server.use("/ftp", ftpRouter);

server.start({ port: process.env.PORT }, () => console.log(`Server is running on ` + process.env.LOCALPRISMAENDPOINT));
