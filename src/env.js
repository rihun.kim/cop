import dotenv from "dotenv";
import path from "path";

dotenv.config({
  path: path.resolve(path.parse(__dirname).dir, ".env")
});
