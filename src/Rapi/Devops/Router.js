import express from "express";

import buildAndroid from "./BuildAndroid/BuildAndroid";

const router = express.Router();

router.post("/", async (req, res) => {
  const { action } = req.body;

  if (action === "buildandroid") {
    buildAndroid(req, res, req.body);
  } else {
    res.send({
      status: "error",
      desc: "Not right action or parameters",
    });
  }
});

export default router;
