import { exec, execSync } from "child_process";
import fs from "fs";

import "../../../env";
import { pushSuccessMessage } from "./Tool";
import { isAuthenticated } from "../../../utils";

const buildUserAndroid = async (req, res, { buildmode, projectid }) => {
  isAuthenticated(req);

  const rnProjectPath = process.env.FTPENDPOINT + "Users/" + req.client.id + "/Projects/" + projectid + "/";
  const rnProjectSourcePath = rnProjectPath + "Source/";
  const projectName = fs.readdirSync(rnProjectSourcePath);
  const rnProjectAndroidPath = rnProjectSourcePath + projectName[0] + "/android/";
  const fastlanePath = process.env.FTPENDPOINT + "Fast/";

  res.send({
    status: "done",
  });

  initFastlane(buildmode, rnProjectAndroidPath, fastlanePath);
  moveApk(rnProjectPath, rnProjectAndroidPath);
  pushSuccessMessage(req.client.pushToken);
};

const moveApk = (rnProjectPath, rnProjectAndroidPath) => {
  const newApkPath = rnProjectPath + "Apks/";
  const oldApkPath = rnProjectAndroidPath + "app/build/outputs/apk/debug/app-debug.apk";

  execSync("cp " + oldApkPath + " " + newApkPath);
};

const initFastlane = (buildmode, rnProjectAndroidPath, fastlanePath) => {
  try {
    if (!fs.existsSync(rnProjectAndroidPath + "/fastlane/Fastfile")) {
      execSync("sh ./fastlaneBuild.sh fastlane init" + " " + rnProjectAndroidPath, { cwd: fastlanePath, stdio: "inherit" });
    }
    applyFastlane(buildmode, rnProjectAndroidPath, fastlanePath);
  } catch (e) {
    console.log(e);
  }
};

const applyFastlane = (buildmode, rnProjectAndroidPath, fastlanePath) => {
  if (buildmode === "release") {
    execSync("sh ./fastlaneBuild.sh fastlane build_release" + " " + rnProjectAndroidPath, { cwd: fastlanePath, stdio: "inherit" });
  } else if (buildmode === "debug") {
    execSync("sh ./fastlaneBuild.sh fastlane build_debug" + " " + rnProjectAndroidPath, { cwd: fastlanePath, stdio: "inherit" });
  }
};

export default buildUserAndroid;
