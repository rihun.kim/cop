import httpRequest from "request";

export const pushSuccessMessage = (pushToken) => {
  httpRequest.post(
    {
      headers: { "content-type": "application/json" },
      url: "http://192.168.13.191:7300/push",
      body: {
        action: "pushhostmessage",
        to: pushToken,
        title: "안녕하세요.",
        body: "APK 가 성공적으로 생성되었습니다.",
      },
      json: true,
    },
    (error, res, body) => {
      console.log(body);
    }
  );
};

export const startRnps = (projectid) => {
  httpRequest.post(
    {
      headers: { "content-type": "application/json" },
      url: "http://192.168.13.191:7300/debug",
      body: {
        action: "debugapp",
        projectid: projectid,
      },
      json: true,
    },
    (error, res, body) => {
      console.log(body);
    }
  );
};
