import { exec, execSync } from "child_process";
import fs from "fs";

import "../../../env";

const buildAnonAndroid = async (req, res, { buildmode, projectid }) => {
  const rnProjectPath = process.env.FTPENDPOINT + "Anon/Projects/" + projectid + "/";
  const rnProjectSourcePath = rnProjectPath + "/Source/";
  const projectName = fs.readdirSync(rnProjectSourcePath);
  const rnProjectAndroidPath = rnProjectSourcePath + projectName[0] + "/android/";
  const fastlanePath = process.env.FTPENDPOINT + "Fast/";

  initFastlane(buildmode, rnProjectAndroidPath, fastlanePath);
  // RNPS 을 켜고 noti를 보내야됨
  res.send({
    status: "done",
  });
};

const initFastlane = (buildmode, rnProjectAndroidPath, fastlanePath) => {
  try {
    if (!fs.existsSync(rnProjectAndroidPath + "/fastlane/Fastfile")) {
      execSync("sh ./fastlaneBuild.sh fastlane init" + " " + rnProjectAndroidPath, { cwd: fastlanePath, stdio: "inherit" });
    }
    applyFastlane(buildmode, rnProjectAndroidPath, fastlanePath);
  } catch (e) {
    console.log(e);
  }
};

const applyFastlane = (buildmode, rnProjectAndroidPath, fastlanePath) => {
  if (buildmode === "release") {
    execSync("sh ./fastlaneBuild.sh fastlane build_release" + " " + rnProjectAndroidPath, { cwd: fastlanePath, stdio: "inherit" });
  } else if (buildmode === "debug") {
    execSync("sh ./fastlaneBuild.sh fastlane build_debug" + " " + rnProjectAndroidPath, { cwd: fastlanePath, stdio: "inherit" });
  }
};

export default buildAnonAndroid;
