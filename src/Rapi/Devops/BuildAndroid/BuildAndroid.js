import { isAnonymous } from "../../../utils";
import buildAnonAndroid from "./BuildAnonAndroid";
import buildUserAndroid from "./BuildUserAndroid";

const buildAndroid = (req, res, { buildmode, projectid }) => {
  isAnonymous(req) ? buildAnonAndroid(req, res, { buildmode, projectid }) : buildUserAndroid(req, res, { buildmode, projectid });
};

export default buildAndroid;
