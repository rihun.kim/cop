import fs from "fs";
import mime from "mime";

import "../../../env";
import { isAuthenticated } from "../../../utils";

const Downloader = async (req, res, { projectid }) => {
  isAuthenticated(req);

  const apkName = "app-debug.apk";
  const apk = process.env.FTPENDPOINT + "Users/" + req.client.id + "/Projects/" + projectid + "/Apks/" + apkName;

  const mimeType = mime.lookup(apk);

  try {
    let apkDownloadStream = fs.createReadStream(apk);

    res.writeHead(200, {
      "Content-Type": mimeType,
      "Content-Disposition": "attachment; filename=" + apkName,
    });
    apkDownloadStream.on("data", (data) => {
      res.write(data);
    });
    apkDownloadStream.on("end", () => {
      res.end();
    });
    apkDownloadStream.on("error", (error) => {
      res.end();
    });
  } catch (e) {
    console.log(e);
  }
};

export default Downloader;
