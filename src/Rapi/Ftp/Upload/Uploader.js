import express from "express";
import multer from "multer";

import "../../../env";
import { isAuthenticated } from "../../../utils";

const Uploader = express.Router();
const storage = multer.diskStorage({ destination: process.env.FTPENDPOINT, filename: (req, file, cb) => cb(null, file.originalname) });
const mul = multer({ storage: storage });

Uploader.post("/", mul.single("resoure"), async (req, res) => {
  // isAuthenticated(req);

  res.send("hi");
});

export default Uploader;
