import express from "express";

import downloader from "./Download/Downloader";
import uploader from "./Upload/Uploader";

const router = express.Router();

router.post("/", async (req, res) => {
  const { action } = req.body;

  if (action === "downloadapk") {
    downloader(req, res, req.body);
  } else if (action === "uploadapk") {
    uploader(req, res, req.body);
  } else {
    res.send({
      status: "error",
      desc: "Not right action or parameters",
    });
  }
});

export default router;
