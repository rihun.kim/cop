import { exec, execSync, spawn, spawnSync } from "child_process";

import "../../env";

const launcher = async (req, res, { projectid }) => {
  const rnProjectPath = process.env.FTPENDPOINT + "Users/" + req.client.id + "/Projects/" + projectid + "/";
  const rnProjectSourcePath = rnProjectPath + "Source/";
  const projectName = fs.readdirSync(rnProjectSourcePath);
  const rnProjectNamePath = rnProjectSourcePath + projectName[0];

  const rnps = spawn("react-native", ["start"], { cwd: rnProjectNamePath });
  rnps.stdout.on("data", (data) => {
    console.log(data.toString());
  });

  // 설치
  // console.log("::: react-native run-android 시작");
  // const installProcess = spawn("react-native", ["run-android"], { cwd: rnProjectPath });
  // installProcess.stdout.on("data", (data) => {
  //   console.log("::: ", data.toString());
  // });

  res.end("debugging");
};

export default launcher;
