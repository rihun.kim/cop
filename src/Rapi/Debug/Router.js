import express from "express";

import launcher from "./Launcher";

const router = express.Router();

router.post("/", async (req, res) => {
  const { action } = req.body;

  if (action === "debugapp") {
    launcher(req, res, req.body);
  } else {
    res.send({
      status: "error",
      desc: "Not right action or parameters",
    });
  }
});

export default router;
