import { request } from "graphql-request";

import "../../../../env";

const createInstance = (req, res, { projectid, platform }) => {
  const createInstanceMutation = `
    mutation createInstance($projectId: String!, $platform: String!) {
      createInstance(projectId: $projectId, platform: $platform) {
        id
        project {
          id
          name
        }
        platform
        pushToken
        pushType
      }
    }
  `;

  const variables = {
    projectId: projectid,
    platform: platform,
  };

  res.set("Content-Type", "application/json");
  request(process.env.LOCALPRISMAENDPOINT, createInstanceMutation, variables)
    .then((data) => {
      res.send({
        status: "done",
        project: data.createInstance.project,
        instanceId: data.createInstance.id,
        platform: data.createInstance.platform,
        pushToken: data.createInstance.pushToken,
        pushType: data.createInstance.pushType,
      });
    })
    .catch((e) => {
      res.send({
        status: "error",
        desc: e,
      });
    });
};

export default createInstance;
