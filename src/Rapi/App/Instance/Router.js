import express from "express";

import createInstance from "./CreateInstance/CreateInstance";
import updateInstance from "./UpdateInstance/UpdateInstance";
import readInstance from "./ReadInstance/ReadInstance";

const router = express.Router();

router.post("/", async (req, res) => {
  const { action } = req.body;

  if (action === "createinstance") {
    createInstance(req, res, req.body);
  } else if (action === "readinstance") {
    readInstance(req, res, req.body);
  } else if (action === "updateinstance") {
    updateInstance(req, res, req.body);
  } else {
    res.send({
      status: "error",
      desc: "Not right action or parameters",
    });
  }
});

export default router;
