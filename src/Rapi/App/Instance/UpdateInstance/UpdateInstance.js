import { request } from "graphql-request";

import "../../../../env";

const updateInstance = (req, res, { instanceid, platform, pushtoken, pushtype }) => {
  const updateInstanceMutation = `
    mutation updateInstance($instanceId: String!, $platform: String, $pushToken: String, $pushType: String) {
      updateInstance(instanceId: $instanceId, platform: $platform, pushToken: $pushToken, pushType: $pushType) {
        id
        project {
          id
          name
        }
        platform
        pushToken
        pushType
      }
    }
  `;

  const variables = {
    instanceId: instanceid,
    platform: platform,
    pushToken: pushtoken,
    pushType: pushtype,
  };

  res.set("Content-Type", "application/json");
  request(process.env.LOCALPRISMAENDPOINT, updateInstanceMutation, variables)
    .then((data) => {
      res.send({
        status: "done",
        instanceId: data.updateInstance.id,
        project: data.updateInstance.project,
        platform: data.updateInstance.platform,
        pushToken: data.updateInstance.pushToken,
        pushType: data.updateInstance.pushType,
      });
    })
    .catch((e) => {
      res.send({
        status: "error",
        desc: e,
      });
    });
};

export default updateInstance;
