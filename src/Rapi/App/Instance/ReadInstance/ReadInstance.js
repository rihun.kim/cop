import { request } from "graphql-request";

import "../../../../env";

const readInstance = (req, res, { instanceid }) => {
  const readInstanceQuery = `
    query readInstance($instanceId: String!) {
      readInstance(instanceId: $instanceId) {
        id
        project {
          id
          name
        }
        platform
        pushToken
        pushType
      }
    }
  `;

  const variables = {
    instanceId: instanceid,
  };

  res.set("Content-Type", "application/json");
  request(process.env.LOCALPRISMAENDPOINT, readInstanceQuery, variables)
    .then((data) => {
      res.send({
        status: "done",
        instanceId: data.readInstance.id,
        project: data.readInstance.project,
        platform: data.readInstance.platform,
        pushToken: data.readInstance.pushToken,
        pushType: data.readInstance.pushType,
      });
    })
    .catch((e) => {
      res.send({
        status: "error",
        desc: e,
      });
    });
};

export default readInstance;
