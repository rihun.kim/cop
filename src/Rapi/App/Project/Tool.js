// import httpRequest from "request";
import fs from "fs";

export const makeProjectDirectory = (projectPath) => {
  if (!fs.existsSync(projectPath)) {
    fs.mkdirSync(projectPath + "/Source", { recursive: true });
    fs.mkdirSync(projectPath + "/Icons", { recursive: true });
    fs.mkdirSync(projectPath + "/Certificates", { recursive: true });
    fs.mkdirSync(projectPath + "/Apks", { recursive: true });
  }
};

const removeDir = (path) => {
  const files = fs.readdirSync(path);

  if (files.length > 0) {
    files.forEach(function(filename) {
      if (fs.statSync(path + "/" + filename).isDirectory()) {
        removeDir(path + "/" + filename);
      } else {
        fs.unlinkSync(path + "/" + filename);
      }
    });
    fs.rmdirSync(path);
  } else {
    fs.rmdirSync(path);
  }

  return true;
};

export const removeProjectDirectory = (projectPath) => {
  if (fs.existsSync(projectPath)) {
    return new Promise((resolve, reject) => {
      resolve(removeDir(projectPath));
    });
  }
};

// export const requestAnonBuildAndroid = (projectId, buildMode) => {
//   httpRequest.post(
//     {
//       headers: { "content-type": "application/json" },
//       // url: "http://192.168.13.191:7300/devops",
//       url: "http://192.168.13.191:3333/devops",
//       body: {
//         action: "buildandroid",
//         buildmode: buildMode,
//         projectid: projectId,
//       },
//       json: true,
//     },
//     (error, res, body) => {
//       console.log(body);
//     }
//   );
// };
