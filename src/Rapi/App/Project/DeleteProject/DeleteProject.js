import { request } from "graphql-request";

import "../../../../env";
import { isAuthenticated } from "../../../../utils";
import { removeProjectDirectory } from "../Tool";

const deleteProject = async (req, res, { projectid }) => {
  isAuthenticated(req);

  const deleteProjectMutation = `
    mutation deleteProject($projectId: String!) {
      deleteProject(projectId: $projectId) {
        id
      }
    }
  `;

  const variables = {
    projectId: projectid,
  };

  res.set("Content-Type", "application/json");
  request(process.env.LOCALPRISMAENDPOINT, deleteProjectMutation, variables)
    .then(async (data) => {
      const projectPath = process.env.FTPENDPOINT + "Users/" + req.client.id + "/Projects/" + data.deleteProject.id;
      await removeProjectDirectory(projectPath);

      res.send({
        status: "done",
      });
    })
    .catch((e) => {
      res.send({
        status: "error",
        desc: e,
      });
    });
};

export default deleteProject;
