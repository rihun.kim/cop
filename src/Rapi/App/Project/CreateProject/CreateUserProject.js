import { request } from "graphql-request";

import "../../../../env";
import { isAuthenticated } from "../../../../utils";
import { makeProjectDirectory } from "../Tool";

const createUserProject = (req, res, { projectname }) => {
  isAuthenticated(req);

  const createProjectMutation = `
    mutation createProject($userId: String!, $projectName: String!) {
      createProject(userId: $userId, projectName: $projectName) {
        id
        name
      }
    }
  `;

  const variables = {
    userId: req.client.id,
    projectName: projectname,
  };

  res.set("Content-Type", "application/json");
  request(process.env.LOCALPRISMAENDPOINT, createProjectMutation, variables)
    .then((data) => {
      const projectPath = process.env.FTPENDPOINT + "Users/" + variables.userId + "/Projects/" + data.createProject.id;
      makeProjectDirectory(projectPath);

      res.send({
        status: "done",
        id: data.createProject.id,
        name: data.createProject.name,
      });
    })
    .catch((e) => {
      res.send({
        status: "error",
        desc: e,
      });
    });
};

export default createUserProject;
