import { request } from "graphql-request";

import "../../../../utils";
import { makeProjectDirectory } from "../Tool";

const createAnonProject = (req, res, { projectname }) => {
  const createAnonProjectMutation = `
    mutation createAnonProject($projectName: String!) {
      createAnonProject(projectName: $projectName) {
        id
        name
      }
    }
  `;

  const variables = {
    projectName: projectname,
  };

  res.set("Content-Type", "application/json");
  request(process.env.LOCALPRISMAENDPOINT, createAnonProjectMutation, variables)
    .then((data) => {
      const projectPath = process.env.FTPENDPOINT + "Anon/Projects/" + data.createAnonProject.id;
      makeProjectDirectory(projectPath);

      res.send({
        status: "done",
        id: data.createAnonProject.id,
        name: data.createAnonProject.name,
      });
    })
    .catch((e) => {
      res.send({
        status: "error",
        desc: e,
      });
    });
};

export default createAnonProject;
