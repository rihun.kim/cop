import { isAnonymous } from "../../../../utils";
import createAnonProject from "./CreateAnonProject";
import createUserProject from "./CreateUserProject";

const createProject = (req, res, { projectname }) => {
  isAnonymous(req) ? createAnonProject(req, res, { projectname }) : createUserProject(req, res, { projectname });
};

export default createProject;
