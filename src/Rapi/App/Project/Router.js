import express from "express";

import createProject from "./CreateProject/CreateProject";
import readProject from "./ReadProject/ReadProject";
import deleteProject from "./DeleteProject/DeleteProject";

const router = express.Router();

router.post("/", async (req, res) => {
  const { action } = req.body;

  if (action === "createproject") {
    createProject(req, res, req.body);
  } else if (action === "readproject") {
    readProject(req, res, req.body);
  } else if (action === "deleteproject") {
    deleteProject(req, res, req.body);
  } else {
    res.send({
      status: "error",
      desc: "Not right action or parameters",
    });
  }
});

export default router;
