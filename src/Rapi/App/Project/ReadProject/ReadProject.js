import { request } from "graphql-request";

import "../../../../env";
import { isAuthenticated } from "../../../../utils";

const readProject = (req, res, { projectid }) => {
  isAuthenticated(req);

  const readProjectQuery = `
    query readProject($projectId: String!) {
      readProject(projectId: $projectId) {
        id
        name
        instances {
          id
          platform
          pushToken
          pushType
        }
      }
    }
  `;

  const variables = {
    projectId: projectid,
  };

  res.set("Content-Type", "application/json");
  request(process.env.LOCALPRISMAENDPOINT, readProjectQuery, variables)
    .then((data) => {
      res.send({
        status: "done",
        id: data.readProject.id,
        name: data.readProject.name,
        instances: data.readProject.instances,
      });
    })
    .catch((e) => {
      res.send({
        status: "error",
        desc: e,
      });
    });
};

export default readProject;
