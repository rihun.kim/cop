import express from "express";

import createUser from "./CreateUser/CreateUser";
import requestUserToken from "./RequestUserToken/RequestUserToken";
import readUser from "./ReadUser/ReadUser";
import updateUser from "./UpdateUser/UpdateUser";

const router = express.Router();

router.post("/", async (req, res) => {
  const { action } = req.body;

  if (action === "createuser") {
    createUser(req, res, req.body);
  } else if (action === "requestusertoken") {
    requestUserToken(req, res, req.body);
  } else if (action === "readuser") {
    readUser(req, res);
  } else if (action === "updateuser") {
    updateUser(req, res, req.body);
  } else {
    res.send({
      status: "error",
      desc: "Not right action or parameters",
    });
  }
});

export default router;
