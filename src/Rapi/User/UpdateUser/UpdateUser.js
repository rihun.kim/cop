import { request } from "graphql-request";

import { isAuthenticated } from "../../../utils";
import "../../../env";

const updateUser = (req, res, { email, password, pushToken }) => {
  isAuthenticated(req);

  const updateUserMutation = `
    mutation updateUser($userId: String!, $email: String, $password: String, $pushToken: String) {
      updateUser(userId: $userId, email: $email, password: $password, pushToken: $pushToken) {
        id
        email
        pushToken
      }
    }
  `;

  const variables = {
    userId: req.client.id,
    email: email,
    password: password,
    pushToken: pushToken,
  };

  res.set("Content-Type", "application/json");
  request(process.env.LOCALPRISMAENDPOINT, updateUserMutation, variables)
    .then((data) => {
      res.send({
        status: "done",
        id: data.updateUser.id,
        email: data.updateUser.email,
        pushToken: data.updateUser.pushToken,
      });
    })
    .catch((e) => {
      res.send({
        status: "error",
        desc: e,
      });
    });
};

export default updateUser;
