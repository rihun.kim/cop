import { request } from "graphql-request";

import "../../../env";

const createUser = (req, res, { email, password }) => {
  const createUserMutation = `
    mutation createUser($email: String!, $password: String!) {
      createUser(email: $email, password: $password) {
        id
        email
        pushToken
      }
    }
  `;

  const variables = {
    email: email,
    password: password,
  };

  res.set("Content-Type", "application/json");
  request(process.env.LOCALPRISMAENDPOINT, createUserMutation, variables)
    .then((data) => {
      res.send({
        status: "done",
        id: data.createUser.id,
        email: data.createUser.email,
      });
    })
    .catch((e) => {
      res.send({
        status: "error",
        desc: e,
      });
    });
};

export default createUser;
