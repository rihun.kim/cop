import { request } from "graphql-request";

import "../../../env";

const requestUserToken = (req, res, { email, password }) => {
  const requestUserTokenMutation = `
    mutation requestUserToken($email: String!, $password: String!) {
      requestUserToken(email: $email, password: $password) 
    }
  `;

  const variables = {
    email: email,
    password: password,
  };

  res.set("Content-Type", "application/json");
  request(process.env.LOCALPRISMAENDPOINT, requestUserTokenMutation, variables)
    .then((data) => {
      res.send({
        status: "done",
        userToken: data.requestUserToken,
      });
    })
    .catch((e) => {
      res.send({
        status: "error",
        desc: e,
      });
    });
};

export default requestUserToken;
