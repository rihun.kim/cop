import { request } from "graphql-request";

import "../../../env";
import { isAuthenticated } from "../../../utils";

const readUser = (req, res) => {
  isAuthenticated(req);

  const readUserQuery = `
    query readUser($userId: String!) {
      readUser(userId: $userId) {
        id
        email
        pushToken
        projects {
          id
          name
        }
      }
    }
  `;

  const variables = {
    userId: req.client.id,
  };

  res.set("Content-Type", "application/json");
  request(process.env.LOCALPRISMAENDPOINT, readUserQuery, variables)
    .then((data) => {
      res.send({
        status: "done",
        id: data.readUser.id,
        email: data.readUser.email,
        pushToken: data.readUser.pushToken,
        projects: data.readUser.projects,
      });
    })
    .catch((e) => {
      res.send({
        status: "error",
        desc: e,
      });
    });
};

export default readUser;
