import FCM from "fcm-node";

import "../../../env";

const pushHostMessage = async (req, res, { to, title, body }) => {
  try {
    res.set("Content-Type", "application/json");

    const fcm = new FCM(process.env.FCMSERVERKEY);

    const message = {
      to: to,
      notification: {
        title: title,
        body: body,
        sound: "default",
      },
    };

    fcm.send(message, function(error, response) {
      if (error) {
        res.send({
          status: "error",
          desc: error,
        });
      } else {
        res.send({ status: "done" });
      }
    });
  } catch (e) {
    res.send({
      status: "error",
      desc: e,
    });
  }
};

export default pushHostMessage;
