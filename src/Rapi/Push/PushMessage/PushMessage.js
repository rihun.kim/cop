import { request } from "graphql-request";
import FCM from "fcm-node";

import "../../../env";

const pushMessage = async (req, res, { to, title, body }) => {
  const readInstanceQuery = `
    query readInstance($instanceId: String!) {
      readInstance(instanceId: $instanceId) {
        pushToken
      }
    }
  `;

  const variables = {
    instanceId: to,
  };

  res.set("Content-Type", "application/json");
  request(process.env.LOCALPRISMAENDPOINT, readInstanceQuery, variables)
    .then((data) => {
      const fcm = new FCM(process.env.FCMSERVERKEY);

      const message = {
        to: data.readInstance.pushToken,
        notification: {
          title: title,
          body: body,
          sound: "default",
        },
      };

      fcm.send(message, function(error, response) {
        if (error) {
          res.send({
            status: "error",
            desc: error,
          });
        } else {
          res.send({ status: "done" });
        }
      });
    })
    .catch((e) => {
      res.send({
        status: "error",
        desc: e,
      });
    });
};

export default pushMessage;
