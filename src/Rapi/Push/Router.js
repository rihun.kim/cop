import express from "express";

import pushMessage from "./PushMessage/PushMessage";
import pushHostMessage from "./PushMessage/PushHostMessage";

const router = express.Router();

router.post("/", (req, res) => {
  const { action } = req.body;

  if (action === "pushmessage") {
    pushMessage(req, res, req.body);
  } else if (action === "pushhostmessage") {
    pushHostMessage(req, res, req.body);
  } else {
    res.send({
      status: "error",
      desc: "Not right action or parameters",
    });
  }
});

export default router;
