import jwt from "jsonwebtoken";

import "./env";

export const generateToken = (id) => jwt.sign({ id }, process.env.JWT_KEY);

export const isAnonymous = (request) => {
  return request.client ? false : true;
};

export const isAuthenticated = (request) => {
  if (!request.client) {
    throw Error("[ALERT] 먼저 로그인을 해주세요.");
  }
};
