import { prisma } from "../../../../generated/prisma-client";

export default {
  Mutation: {
    updateInstance: async (_, args) => {
      const { instanceId, platform, pushToken, pushType } = args;

      try {
        const isInstanceExisted = await prisma.$exists.instance({
          id: instanceId
        });

        if (isInstanceExisted) {
          return await prisma.updateInstance({
            where: { id: instanceId },
            data: {
              platform: platform,
              pushType: pushType,
              pushToken: pushToken
            }
          });
        } else {
          return null;
        }
      } catch (e) {
        throw Error("[ALERT] 서버쪽에서 에러가 발생하였습니다. ::: " + e);
      }
    }
  }
};
