import { prisma } from "../../../../generated/prisma-client";

export default {
  Query: {
    readInstance: async (_, args) => {
      const { instanceId } = args;

      try {
        const isInstanceExisted = await prisma.$exists.instance({
          id: instanceId
        });

        if (isInstanceExisted) {
          return await prisma.instance({ id: instanceId });
        } else {
          return null;
        }
      } catch (e) {
        throw Error("[ALERT] 서버쪽에서 에러가 발생하였습니다. ::: " + e);
      }
    }
  }
};
