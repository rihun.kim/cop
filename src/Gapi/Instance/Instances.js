import { prisma } from "../../../generated/prisma-client";

export default {
  Instance: {
    id: ({ id }) => id,
    project: ({ id }) => prisma.instance({ id }).project(),
    platform: ({ id }) => prisma.instance({ id }).platform(),
    pushToken: ({ id }) => prisma.instance({ id }).pushToken(),
    pushType: ({ id }) => prisma.instance({ id }).pushType()
  }
};
