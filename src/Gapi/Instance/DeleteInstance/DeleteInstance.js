import { prisma } from "../../../../generated/prisma-client";

export default {
  Mutation: {
    deleteInstance: async (_, args) => {
      const { instanceId } = args;

      try {
        const isInstanceExisted = await prisma.$exists.instance({
          id: instanceId
        });

        if (isInstanceExisted) {
          const result = await prisma.deleteInstance({ id: instanceId });

          if (result) {
            return true;
          } else {
            return false;
          }
        } else {
          return false;
        }
      } catch (e) {
        throw Error("[ALERT] 서버쪽에서 에러가 발생하였습니다. ::: " + e);
      }
    }
  }
};
