import { prisma } from "../../../../generated/prisma-client";

export default {
  Mutation: {
    createInstance: async (_, args) => {
      const { projectId, platform } = args;

      try {
        const isProjectExisted = await prisma.$exists.project({
          id: projectId
        });

        if (isProjectExisted) {
          return await prisma.createInstance({
            project: {
              connect: { id: projectId }
            },
            platform
          });
        } else {
          return null;
        }
      } catch (e) {
        throw Error("[ERROR] 서버쪽에서 에러가 발생하였습니다. ::: " + e);
      }
    }
  }
};
