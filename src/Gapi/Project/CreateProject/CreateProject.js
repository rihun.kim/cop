import { prisma } from "../../../../generated/prisma-client";

export default {
  Mutation: {
    createProject: async (_, args) => {
      const { userId, projectName } = args;

      try {
        return await prisma.createProject({
          user: { connect: { id: userId } },
          name: projectName,
        });
      } catch (e) {
        throw Error("[ERROR] 서버쪽에서 에러가 발생하였습니다. ::: " + e);
      }
    },
    createAnonProject: async (_, args) => {
      const { projectName } = args;

      try {
        return await prisma.createProject({
          name: projectName,
        });
      } catch (e) {
        throw Error("[ERROR] 서버쪽에서 에러가 발생하였습니다. ::: " + e);
      }
    },
  },
};
