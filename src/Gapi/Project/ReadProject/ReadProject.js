import { prisma } from "../../../../generated/prisma-client";

export default {
  Query: {
    readProject: async (_, args) => {
      const { projectId } = args;

      try {
        const isProjectExisted = await prisma.$exists.project({
          id: projectId,
        });

        if (isProjectExisted) {
          return await prisma.project({ id: projectId });
        } else {
          return null;
        }
      } catch (e) {
        throw Error("[ERROR] 서버쪽에서 에러가 발생하였습니다. ::: " + e);
      }
    },
  },
};
