import { prisma } from "../../../generated/prisma-client";

export default {
  Project: {
    id: ({ id }) => id,
    user: ({ id }) => prisma.project({ id }).user(),
    name: ({ id }) => prisma.project({ id }).name(),
    instances: ({ id }) => prisma.project({ id }).instances(),
  },
};
