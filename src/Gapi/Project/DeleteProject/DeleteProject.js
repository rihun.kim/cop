import { prisma } from "../../../../generated/prisma-client";

export default {
  Mutation: {
    deleteProject: async (_, args) => {
      const { projectId } = args;

      try {
        return await prisma.deleteProject({ id: projectId });
      } catch (e) {
        throw Error("[ERROR] 서버쪽에서 에러가 발생하였습니다. ::: " + e);
      }
    },
  },
};
