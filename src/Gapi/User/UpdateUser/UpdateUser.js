import { prisma } from "../../../../generated/prisma-client";

export default {
  Mutation: {
    updateUser: async (_, args) => {
      const { userId, email, password, pushToken } = args;

      try {
        const isUserExisted = await prisma.$exists.user({
          id: userId,
        });

        if (isUserExisted) {
          return await prisma.updateUser({ where: { id: userId }, data: { email: email, password: password, pushToken: pushToken } });
        } else {
          return null;
        }
      } catch (e) {
        throw Error("[ERROR] 서버쪽에서 에러가 발생하였습니다. ::: " + e);
      }
    },
  },
};
