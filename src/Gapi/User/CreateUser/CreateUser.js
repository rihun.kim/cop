import { prisma } from "../../../../generated/prisma-client";

export default {
  Mutation: {
    createUser: async (_, args) => {
      const { email, password } = args;

      try {
        const isUserExisted = await prisma.$exists.user({
          email: email,
        });

        if (!isUserExisted) {
          return await prisma.createUser({
            email: email,
            password: password,
          });
        } else {
          return null;
        }
      } catch (e) {
        throw Error("[ERROR] 서버쪽에서 에러가 발생하였습니다. ::: " + e);
      }
    },
  },
};
