import { prisma } from "../../../../generated/prisma-client";

export default {
  Query: {
    readUser: async (_, args) => {
      const { userId } = args;

      try {
        const isUserExisted = await prisma.$exists.user({
          id: userId,
        });

        if (isUserExisted) {
          return await prisma.user({ id: userId });
        } else {
          return null;
        }
      } catch (e) {
        throw Error("[ERROR] 서버쪽에서 에러가 발생하였습니다. ::: " + e);
      }
    },
  },
};
