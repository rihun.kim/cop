import { prisma } from "../../../generated/prisma-client";

export default {
  User: {
    id: ({ id }) => id,
    email: ({ id }) => prisma.user({ id }).email(),
    pushToken: ({ id }) => prisma.user({ id }).pushToken(),
    projects: ({ id }) => prisma.user({ id }).projects(),
  },
};
