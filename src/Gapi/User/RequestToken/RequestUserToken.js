import { prisma } from "../../../../generated/prisma-client";
import { generateToken } from "../../../utils";

export default {
  Mutation: {
    requestUserToken: async (_, args) => {
      const { email, password } = args;

      try {
        const isEmailExisted = await prisma.$exists.user({
          email: email,
        });

        if (isEmailExisted) {
          const user = await prisma.user({ email: email });

          if (user.password === password) {
            return generateToken(user.id);
          } else {
            return "[ALERT] 비밀번호를 잘못 입력하셨습니다.";
          }
        } else {
          return "[ALERT] 이메일을 잘못 입력하셨습니다.";
        }
      } catch (e) {
        throw Error("[ERROR] 서버쪽에서 에러가 발생하였습니다. ::: " + e);
      }
    },
  },
};
